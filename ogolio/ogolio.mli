
(** Interface to the OGoLIO library

   MIT License

   Copyright (c) 2021 Tim Whelan
*)

(** Save a board *)
val save_board : int array array -> string -> int

